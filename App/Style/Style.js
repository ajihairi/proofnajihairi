import { StyleSheet } from 'react-native';
'use strict';

module.exports = StyleSheet.create({
  header: {
    flex: 1,
    backgroundColor: '#3498db', 
    borderBottomWidth: 2, 
    borderBottomColor: '#2980b9',
  },
  iconheader: {
      color: 'white',
  },
  textheader: {
      color: 'white',
  },
  splashScreens:{
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  textStylesSplashScreen:{
    fontStyle: 'italic'
  },
  imgStyleSplashScreen:{
    flex: 1,
    width: undefined,
    height: undefined,
    alignSelf: 'stretch'
  },
  commonTextStyleButton: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
},
commonButtonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#007aff',
    marginLeft: 5,
    marginRight: 5
},
cardContainerStyle: {
  borderRadius: 5,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  elevation: 1,
  marginLeft: 5,
  marginRight: 5,
  marginTop: 10
},
cardSectionContainerStyle: {
  borderRadius: 5,
  padding: 5,
  marginBottom: 10,
  backgroundColor: 'rgba(255, 255, 255, 0.5)',
  justifyContent: 'center',
  alignItems: 'center',
  paddingLeft: 8,
  flexDirection: 'row',
  position: 'relative'
},
ConfirmCardSectionStyle : {
  padding : 15,
  borderBottomWidth : 0
},
ConfirmTitleStyle :{
  fontSize :20,
  color : '#007aff',
  padding : 15
},
ConfirmTextStyle : {
  flex : 1,
  fontSize : 18,
  textAlign : 'center',
  lineHeight : 40
},
ConfirmContainerStyle : {
  backgroundColor : 'rgba(0, 0, 0, 0.75)',
  position : 'relative',
  flex : 1,
  justifyContent : 'center'
},
ConfirmInsideContainerStyle : {
  backgroundColor : 'rgba(255, 255, 255, 1)',
  margin : 20,
  justifyContent : 'center'
},
header: {
  flexDirection: 'row',
  paddingTop: 10,
  paddingHorizontal: 10,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#ffa216',
  height: 60
},
headerText: {
  fontSize: 18,
  fontWeight: 'bold',
  color: '#fff',
  textAlign: 'center',
  alignSelf:'center'
  
},
HeaderViewStyle: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  alignSelf: 'center',
  textAlign: 'center',
  fontSize: 16,
  paddingTop: 15,
  backgroundColor: 'orange',
  // shadowColor: '#000',
  // shadowOffset: { width: 0, height: 3 },
  // shadowOpacity: 0.1,
  // elevation: 2,
  position: 'relative'
},
HeaderTextStyle: {
  alignSelf: 'center',
  color: 'white'
},
inputStyle: {
  color: '#000',
  paddingRight: 20,
  fontSize: 12,
  textAlign: 'left'
},
InputlabelStyle: {
  fontSize: 12
},
InputContainerStyle: {
  height: 50,
  flexDirection: 'row', 
  alignItems: 'center'
},
shapeStyle: {
  borderRadius: 25,
  padding: 5,
  marginBottom: 10,
  backgroundColor: 'rgba(255, 255, 255, 0.5)',
  paddingLeft: 20,
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'stretch',
  position: 'relative',
  height: 40
},

loginShapeStyle: {
  borderRadius: 25,
  padding: 5,
  marginBottom: 10,
  backgroundColor: 'white',
  paddingLeft: 8,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  alignSelf: 'stretch',
  position: 'relative',
  shadowColor: '#000',
  shadowOffset: { width: 2, height: 2 },
  shadowOpacity: 0.1,
  elevation: 2,
  height: 40
},
spinnerStyle: {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center'
},
AuthBgStyle: {
  paddingBottom: 200,
  flex: 1,
  width: null,
  height: null,
  resizeMode: 'cover',
  paddingLeft: 25,
  paddingRight: 25,
  justifyContent: 'center'
},
chatBgStyle: {
  flex: 1,
  // alignItems: 'flex-start',
  resizeMode: 'contain',
  // justifyContent: 'flex-start'
},
FooterTabBg:{
  color: 'orange'
},
FooterTabTextStyle:{
  color: 'white',
  fontSize: 12
}

});
