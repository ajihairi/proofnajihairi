import React, {PureComponent} from 'react';
import * as Animatable from 'react-native-animatable';
import {Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import Spinner from 'react-native-loading-spinner-overlay';

export default class Loading extends PureComponent{
  render() {
    return (
        <Spinner
          visible={this.props.fetching}
          cancelable={true}
          overlayColor='rgba(0, 0, 0, 0.25)'
        />
    );
  }
}
