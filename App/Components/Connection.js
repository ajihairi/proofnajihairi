import React from 'react';
import * as Animatable from 'react-native-animatable';
import {Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import {KittenTheme} from './../Themes/Theme';

export const ConnectionToast = () => {
  return(
    <Animatable.View animation="fadeInDown" style={styles.container}>
      <LinearGradient
        colors={['#A274CA', '#80B0CE']}
        start={{x: 0.5, y: 0}}
        end={{x: 0, y: 0.5}}
        style ={{
          flex: 1,
          justifyContent: 'center',
          borderBottomLeftRadius: 10,
          borderBottomRightRadius: 10
        }}
      >
        <Text style={styles.text}>
          Connecting...
        </Text>
      </LinearGradient>
    </Animatable.View>
  )
};

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    height: 30,
    width: 150,
    marginBottom: 10
  },
  text:{
    color: '#fff', fontSize: 17, fontWeight: 'bold', textAlign: 'center'
  }
});
