// Import a library to help create a component
import React from 'react';
import { Text, View } from 'react-native';
var styling = require ('../../Style/Style');

// Make a component
const Header = (props) => {
    
    return (
        <View style={styling.HeaderViewStyle}>
            <Text style={styling.HeaderTextStyle}>{props.headerText}</Text>
        </View>
    );
};

// Make the component available to other parts of the app
export { Header };