import React from 'react';
import { View, ActivityIndicator } from 'react-native';
var styling = require ('../../Style/Style');

const Spinner = ({ size }) => {
    return (
        <View style={styles.spinnerStyle}>
            <ActivityIndicator size={size || 'large'} />
        </View>
    );
};

const styles = {
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItem: 'center'
    }
};

export { Spinner };