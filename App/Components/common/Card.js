import React from 'react';
import { View } from 'react-native';
var styling = require ('../../Style/Style'); 
const Card = (props) => {
    return (
        <View style={styling.cardContainerStyle}>
            {props.children}
        </View>
    );
};

export { Card };