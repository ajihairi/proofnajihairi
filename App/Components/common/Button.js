import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

var styling = require ('../../Style/Style');

const Button = ({ onPress, children }) => {

    return (
        <TouchableOpacity 
            onPress={onPress} style={styling.commonButtonStyle}>
            <Text style={styling.commonTextStyleButton}>{children}
            </Text>
        </TouchableOpacity>
    );
};


export { Button };