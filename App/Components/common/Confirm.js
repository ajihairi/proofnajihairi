/*
code by hamzhya tinnov (c) 2017
*/
import React from 'react';
import {
    View,
    Text,
    Modal
} from 'react-native';
import { CardSection } from './CardSection';
import { Button } from './Button';
var styling = require ('../../Style/Style'); 

const Confirm = ({ children, visible, onAccept, onDecline, title }) => {
    return(
        <Modal
            visible={visible}
            transparent
            animationType= "slide"
            onRequestClose = {() => {}}
        >
            <View
                style = {styling.ConfirmContainerStyle}
            >
                <View style = {styling.ConfirmInsideContainerStyle}>
                    <CardSection>
                        <Text style = {styling.ConfirmTitleStyle}>{title}</Text>
                    </CardSection>
                    <CardSection style = {styling.ConfirmCardSectionStyle}>
                        <Text style = {textStyle}>{children}</Text>
                    </CardSection>
                    <CardSection style = {styling.ConfirmCardSectionStyle}>
                        <Button onPress = {onAccept}>Yes</Button>
                        <Button onPress = {onDecline}>No</Button>
                    </CardSection>
                </View>
            </View>
        </Modal>
    );
};


export { Confirm };
