import React from 'react';
import { View } from 'react-native';
var styling = require ('../../Style/Style'); 
const CardSection = (props) => {
    return (
        <View style={styling.cardSectionContainerStyle}>
            {props.children}
        </View>
    );
};

const styles = {
   
};

export { CardSection };