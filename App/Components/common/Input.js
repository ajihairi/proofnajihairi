import React from 'react';
import { TextInput, View, Text } from 'react-native';
var styling = require ('../../Style/Style');

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
    
    return (
        <View style={styling.InputContainerStyle}>
            <Text style={styling.InputlabelStyle}>{label}</Text>
            <TextInput
                autoCapitalize="none"
                secureTextEntry= {secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                style={styling.inputStyle}
                value={value}
                onChangeText={onChangeText}
            />
        </View>
    );
};

export { Input };