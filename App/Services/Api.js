import apisauce from 'apisauce'

const create = (baseURL = 'https://beta.proofn.com/v1/') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 30000
  });

  const postLogin = (payload) => api.post('auth/login', payload);
  const getMessage = () => api.get('messages/inbox');
  const getMessageDetail = (body) => api.get(`messages/inbox/${body}`);
  const postSendMessage = (payload) => api.post('messages/send', payload);
  const deleteMessage = (idx) => api.post(`messages/inbox/${idx}/trash`);
  const getProfile = () => api.get('user/profile');
  const putProfile = (payload) => api.put('user/profile', payload);  

  return {
    api,
    
    postLogin,
    getMessage,
    getMessageDetail,
    postSendMessage,
    deleteMessage,
    getProfile,
    putProfile,

  }
};

export default {
  create
}
