import React from 'react'
import { StackNavigator, TabNavigator, TabBarBottom, HeaderBackButton } from 'react-navigation'
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';
import {Image, View, Text, Animated, Easing} from 'react-native'
import {Platform} from 'react-native';

import {bootstrap} from './../Themes/Bootstrap';

/* --------------------- Proofn ---------------------- */
import LoginScreen from '../Screens/Auth/Login'
import AdminHomeScreen from './../Screens/Home'
import EditProfileScreen from './../Screens/Profile/editProfile'
import MessageDetailScreen from './../Screens/Inbox/messageDetail';
import ComposeScreen from './../Screens/Compose';

/* --------------------- Proofn ---------------------- */



bootstrap();

const navigationOptions = ({ navigation }) => ({
  headerLeft: <HeaderBackButton onPress={() => navigation.goBack(null)} />,
});
const PrimaryNav = StackNavigator( {


/* --------------------- Proofn ---------------------- */
  Login: { screen: LoginScreen },
  AdminHome: {
    screen: AdminHomeScreen,
    navigationOptions: {
      header: null
    },
  },
  EditProfile: {
    screen: EditProfileScreen,
    navigationOptions: {
      header: null
    },
  },
  MessageDetail: {
    screen: MessageDetailScreen,
    navigationOptions: {
      header: null
    },
  },
  ComposeScreen:{
    screen: ComposeScreen,
    navigationOptions:{
      header: null
    }
  }

/* --------------------- Proofn ---------------------- */

}, {
  headerMode: 'screen',
  transitionConfig: ()=> {
    return {
      transitionSpec: {
        duration: 350,
        easing: Easing.out(Easing.poly(5)),
        timing: Animated.timing,
        useNativeDriver: true,
      },
      screenInterpolator: sceneProps => {
        const {position, layout, scene, index, scenes} = sceneProps
        const toIndex = index
        const thisSceneIndex = scene.index
        const height = layout.initHeight
        const width = layout.initWidth
  
        const translateX = position.interpolate({
          inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
          outputRange: [width, 0, 0]
        })
  
        // Since we want the card to take the same amount of time
        // to animate downwards no matter if it's 3rd on the stack
        // or 53rd, we interpolate over the entire range from 0 - thisSceneIndex
        const translateY = position.interpolate({
          inputRange: [0, thisSceneIndex],
          outputRange: [height, 0]
        })
  
        const slideFromRight = {transform: [{translateX}]}
        const slideFromBottom = {transform: [{translateY}]}
  
        const lastSceneIndex = scenes[scenes.length - 1].index
  
        // Test whether we're skipping back more than one screen
        if (lastSceneIndex - toIndex > 1) {
          // Do not transoform the screen being navigated to
          if (scene.index === toIndex) return
          // Hide all screens in between
          if (scene.index !== lastSceneIndex) return {opacity: 0}
          // Slide top screen down
          return slideFromBottom
        }
  
        return slideFromRight
      },
    }
  },
  transitionConfig: ()=> {
    return {screenInterpolator: CardStackStyleInterpolator.forFade}
  },
  // transitionConfig: getSlideFromRightTransition,
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#ffa216',
    },
    headerTitleStyle: {
      color: '#686869'
    }
  }
});

export default PrimaryNav
