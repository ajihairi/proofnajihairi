import React from 'react'
import * as ReactNavigation from 'react-navigation'
import { BackHandler } from "react-native";
import { connect } from 'react-redux'
import AppNavigation from './AppNavigation'

import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

const addListener = createReduxBoundAddListener("root");

// here is our redux-aware smart component
class ReduxNavigation extends React.Component {
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch, nav } = this.props;
    if (nav.routes.length === 1 && (nav.routes[0].routeName === 'Login')) {
      return false;
    }
    dispatch(ReactNavigation.NavigationActions.back());
    return true;
  };

  render() {
    const { dispatch, nav } = this.props;
    const navigation = ReactNavigation.addNavigationHelpers({
      dispatch,
      state: nav,
      addListener
    });
    return (
        <AppNavigation navigation={navigation} />
    )
  }
}

const mapStateToProps = state => ({ nav: state.nav });

export default connect(mapStateToProps)(ReduxNavigation)
