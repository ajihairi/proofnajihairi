import React, { Component } from "react";
import { StyleSheet, Alert, View, TouchableOpacity } from 'react-native'
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  Right,
  StyleProvider
} from "native-base";


var styling = require('../../Style/Style');
const avatar = require('../../Assets/icons/avatar.png');
const defaultAvatar1 = require("../../Assets/icons/usericon.png");

const datas = [
  {
    img: defaultAvatar1,
    text: "Putra Jaya Mandiri",
    note: "123 members"
  },
  {
    img: defaultAvatar1,
    text: "Ujang Lanay",
    note: "21331 members"
  }
];

class GroupScreen extends Component {
  render() {
    return (
        <Container style={styles.container}>
        <View style={styling.header}>
                        <Left style={{ flex: 1 }}>
                        </Left>
                        <Body style={{ flex: 1 }}>
                            <Title style={styling.headerText}>Group</Title>
                        </Body>
                        <Right style={{ flex: 1 }}>
                            <View flexDirection="row" >
                                <TouchableOpacity onPress={this._searchContact}>
                                    <Icon name="ios-search"
                                        style={{ marginHorizontal: 10, fontSize: 20, color: 'white' }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this._addContact}>
                                    <Icon name="md-add"
                                        style={{ marginHorizontal: 5, fontSize: 20, color: 'white' }} />
                                </TouchableOpacity>
                            </View>
                        </Right>
                </View>

          <Content>
            <List >
              {datas.map((data, i) => (
                <ListItem avatar key={i} onPress={() => Alert.alert(data.note)}>
                  <Left>
                    <Thumbnail small source={data.img} />
                  </Left>
                  <Body>
                    <Text>{data.text}</Text>
                    <Text numberOfLines={1} note>
                      {data.note}
                    </Text>
                  </Body>
                </ListItem>
              ))}
            </List>
          </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFF"
  },
});

export default GroupScreen;