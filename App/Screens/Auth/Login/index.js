import React, {Component} from 'react';
import {Input, Spinner, Card, CardSection} from '../../../Components/common';
import {
  View,
  Image,
  Dimensions,
  StyleSheet,
  Alert,
  Keyboard,
  ImageBackground
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkStyleSheet,
  RkTheme
} from 'react-native-ui-kitten';
import {GradientButton} from '../../../Components/GradientButton/index';
import {scale, scaleModerate, scaleVertical} from '../../../Themes/Scale';
import {connect} from 'react-redux';
import LoginActions from '../../../Redux/Auth/LoginRedux';
import Loading from '../../../Components/Loading';
import {Button, Text} from 'native-base';

import authBg from '../../../Assets/images/loginbg.png';
import proofnLogo from '../../../Assets/images/proofn.png';
import whiteproofn from '../../../Assets/images/whiteproofnlogo.png';

const styling = require('../../../Style/Style');

const Screen = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height
};



class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      identifier: null,
      password: null
    }
  }

  componentWillReceiveProps(nextProps) {
    const {fetching, error, message, token} = nextProps;
    if (fetching === false) {
      if (error && !token) {
        alert(JSON.stringify(message))
      }
    }

  }

  _loginClick = () => {
    const {identifier, password} = this.state;
    this.props.login(identifier, password);
  };

  componentDidMount() {
   //did component mount something?
  }

  render() {
    return (
      <ImageBackground source={authBg} style={styles.screen}>
      <RkAvoidKeyboard
        behavior='padding'
        onStartShouldSetResponder={(e) => true}
        onResponderRelease={(e) => Keyboard.dismiss()}
        style={styles.screen}
      >
      
        <Loading fetching={this.props.fetching}/>
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            height: Screen.height,
            ...StyleSheet.absoluteFillObject
          }}
        />
        <View style={styles.container}>
          <View style={{flex: 1, padding: 20, alignItems: 'center'}}>

            <Image
              source={whiteproofn}
              style={{
                width: 100,
                height: 70,
                resizeMode: 'contain'
              }}
            />
            <Image
              source={proofnLogo}
              style={{
                width: 200,
                height: 200,
                marginBottom: 20,
                resizeMode: "contain"
              }}
            />
            <Text style={{fontSize: 16, fontWeight: 'normal', color: '#fafafa'}}>Silahkan Masuk</Text>
          </View>
          <View style={{
            flex: 1,
            alignItems: 'center',
          }}>
            <RkTextInput
              rkType='rounded'
              autoCapitalize="none"
              placeholder='username / email'
              onChangeText={(identifier) => this.setState({identifier})}
              style={{backgroundColor: 'rgba(255, 255, 255, 0.5)'}}
            />
            <RkTextInput
              rkType='rounded'
              autoCapitalize="none"
              placeholder='Password'
              secureTextEntry={true}
              onChangeText={(password) => this.setState({password})}
              style={{backgroundColor: 'rgba(255, 255, 255, 0.5)'}}
            />
            <Button block onPress={this._loginClick}
            style={{ borderRadius: 25,backgroundColor: '#fff', marginTop: 10}}>
              <Text style={{color: 'orange', fontWeight: 'bold'}}>LOGIN</Text>
            </Button>
          </View>
        </View>
      </RkAvoidKeyboard>
      </ImageBackground>
    )
  }
}

let styles = RkStyleSheet.create(theme => ({
  screen: {
    flex: 1,
    alignItems: 'center',
    // backgroundColor: theme.colors.screen.base
  },
  image: {
    resizeMode: 'cover',
    marginBottom: scaleVertical(10),
  },
  container: {
    flex: 1,
    paddingHorizontal: 17,
    // paddingBottom: scaleVertical(22),
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  footer: {
    justifyContent: 'flex-end',
    flex: 1
  },
  buttons: {
    flexDirection: 'row',
    marginBottom: scaleVertical(24)
  },
  button: {
    marginHorizontal: 14
  },
  save: {
    marginVertical: 9
  },
  textRow: {
    justifyContent: 'center',
    flexDirection: 'row',
  }
}));

const mapStateToProps = (state) => ({
  error: state.auth.error,
  fetching: state.auth.fetching,
  token: state.auth.token,
  message: state.auth.message
});

const mapDispatchToProps = (dispatch) => ({
  login: (identifier, password) => dispatch(LoginActions.loginRequest(identifier, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
