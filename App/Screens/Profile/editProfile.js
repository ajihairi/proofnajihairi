import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, TextInput, Picker, TouchableOpacity, AsyncStorage, Alert, Platform } from 'react-native';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import ProfileActions from '../../Redux/Profile';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import jwtDecode from 'jwt-decode';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Left,
  Body,
  Right,
  StyleProvider
} from "native-base";

var styling = require('../../Style/Style');
const avatar = require('../../Assets/icons/avatar.png');
const defaultAvatar1 = require("../../Assets/icons/usericon.png");

class EditProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      profile: params,
      isImagePickerShowing: false,
    };

    this.update = false;
  }

  componentDidMount() {
    this.props.getProfile()
  }

  componentWillReceiveProps(nextProps) {
    const { fetching, error, message } = nextProps.profile;
    if (!fetching && this.update) {
      if (!error) {
        alert('Success Update Profile');
        this.props.navigation.goBack();
        this.props.getProfile();
        this.update = false;
      } else {
        alert('Failure Update Profile');
        this.update = false;
      }
    }
  }

  _editValue = (key, value) => {
    let profile = Object.assign({}, this.state.profile);
    profile[key] = value;
    this.setState({ profile });
  }

  _saveClick = () => {
    this.props.editProfile(this.state.profile);
    this.update = true
  }

  _cancelClick = () => {
    Alert.alert(
      '',
      'Discard changes?',
      [
        { text: 'Yes', onPress: () => this.props.navigation.goBack() },
        { text: 'No', onPress: () => console.tron.log('No'), style: 'cancel' }
      ],
      { cancelable: false }
    )
  }

  uploadStart2 = (opts) => {
    console.tron.log(opts)

    // AsyncStorage.getItem('token')
    //   .then((token) => {
    //     const user = jwtDecode(token);
    //     fetch("https://beta.proofn.com/v1/user/avatar", {
    //       method: "POST",
    //       headers: {
    //         'Content-Type': 'multipart/form-data',
    //         'Authorization': 'bearer' + `${token}`
    //       },
    //       body: JSON.stringify({
    //         avatarPathSmall,
    //         avatarPathMedium,
    //         avatarPathLarge
    //       })
    //     })
    //       .then((response) => response.json())
    //       .then((responseJson) => {
    //         Alert.alert(responseJson.message);
    //         this.props.navigation.goBack(null);
    //         this.props.getMessage();

    //       }).catch((error) => {
    //         console.error(error);
    //       });
    //   })

    AsyncStorage.getItem('token')
      .then((token) => {
        const user = jwtDecode(token);

        RNFetchBlob.fetch('POST', 'https://beta.proofn.com/v1/user/avatar', {
          Authorization: 'Bearer ' + token,
          'Content-Type': 'multipart/form-data',
        }, [
            {
              name: 'avatar',
              filename: 'avatar.png',
              // Change BASE64 encoded data to a file path with prefix `RNFetchBlob-file://`.
              // Or simply wrap the file path with RNFetchBlob.wrap().
              data: RNFetchBlob.wrap(opts.path)
            },
          ])
          .then((resp) => {
            alert('Success Update Avatar');
            console.tron.log(resp.json())
          })
      });
  }

  _cameraClick = (options) => {
    if (this.state.isImagePickerShowing) {
      return
    }
    this.setState({ isImagePickerShowing: true })
    const imagePickerOptions = {
      takePhotoButtonTitle: null,
      title: 'Upload Photo',
      chooseFromLibraryButtonTitle: 'Choose From Library'
    }
    ImagePicker.showImagePicker(imagePickerOptions, (response) => {
      let didChooseVideo = true
      console.tron.log('ImagePicker response: ', response)
      const { customButton, didCancel, error, path, uri } = response
      if (didCancel) {
        didChooseVideo = false
      }
      if (error) {
        console.tron.warn('ImagePicker error:', response)
        didChooseVideo = false
      }
      // TODO: Should this happen higher?
      this.setState({ isImagePickerShowing: false })
      if (!didChooseVideo) {
        return
      }
      if (Platform.OS === 'android') {
        if (path) { // Video is stored locally on the device
          // TODO: What here?
          this.uploadStart2(Object.assign({ path }, options))
        } else { // Video is stored in google cloud
          // TODO: What here?
          this.props.onVideoNotFound()
        }
      } else {
        this.uploadStart2(Object.assign({ path: uri }, options))
      }
    })
  }

  render() {
    const profile = this.state.profile;
    return (
      <View style={styles.container}>
        <View style={styling.header}>
          <Left style={{ flex: 1 }}>
            <TouchableOpacity onPress={this._cancelClick} style={{ paddingLeft: 10 }}>
              <Ionicons name={'ios-close'} size={50} color={'#fff'} />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 2 }}>
            <Title style={styling.headerText}>Edit Profile</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <View flexDirection="row" >
              <TouchableOpacity onPress={this._saveClick}>
                <Icon name="ios-checkmark"
                  style={{ marginHorizontal: 10, fontSize: 50, color: 'white' }} />
              </TouchableOpacity>
            </View>
          </Right>
        </View>

        <ScrollView>
          <View style={styles.profile}>
            <Row size={12}>
              <Col sm={2} style={styles.image}>
                <View>
                </View>
              </Col>
              <Col sm={3}>
                <Ionicons name={'ios-happy'} size={75} />
              </Col>
              <Col sm={7}>
                <TouchableOpacity
                  onPress={() => this._cameraClick({
                    url: 'https://beta.proofn.com/v1/user/avatar',
                    type: 'raw'
                  })}
                >
                  <Ionicons name={'ios-camera'} size={25} color={'#ffa216'} />
                </TouchableOpacity>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-person'} size={25} color={'#ffa216'} />
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Firt Name'}
                  value={this.state.profile.firstName}
                  onChangeText={this._editValue.bind(this, 'firstName')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Middle Name'}
                  value={this.state.profile.middleName}
                  onChangeText={this._editValue.bind(this, 'middleName')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Last Name'}
                  value={this.state.profile.lastName}
                  onChangeText={this._editValue.bind(this, 'lastName')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-transgender'} size={25} color={'#ffa216'} />
              </Col>
              <Col sm={10}>
                <Picker
                  selectedValue={this.state.profile.gender}
                  mode="dropdown"
                  onValueChange={(itemValue, itemIndex) => {
                    const newProfile = Object.assign({}, this.state.profile, { gender: itemValue })
                    this.setState({ profile: newProfile });
                  }}
                  underlineColorAndroid={'#cecece'}
                >
                  <Picker.Item label={""} value={0} />
                  <Picker.Item label={"Male"} value={1} />
                  <Picker.Item label={"Female"} value={2} />
                </Picker>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-call'} size={25} color={'#ffa216'} />
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Phone'}
                  value={this.state.profile.phoneNumber}
                  onChangeText={this._editValue.bind(this, 'phoneNumber')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-home'} size={25} color={'#ffa216'} />
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Home Address'}
                  value={this.state.profile.homeAddress}
                  onChangeText={this._editValue.bind(this, 'homeAddress')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-briefcase'} size={25} color={'#ffa216'} />
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Company Name'}
                  value={this.state.profile.office}
                  onChangeText={this._editValue.bind(this, 'office')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
              </Col>
              <Col sm={10}>
                <TextInput
                  placeholder={'Profession'}
                  value={this.state.profile.profession}
                  onChangeText={this._editValue.bind(this, 'profession')}
                  underlineColorAndroid={'#cecece'}
                />
              </Col>
            </Row>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    padding: 10,
    backgroundColor: '#ffa216',
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
  },
  iconHeader: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileHeader: {
    padding: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
    // borderColor: 'black',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
    // borderColor: 'black',
  },
  content: {
    justifyContent: 'center',
  },
  textHeader: {
    color: '#ffa216',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10
  },
  textContent: {
    fontWeight: 'bold'
  },
  profile: {
    padding: 10,
    backgroundColor: '#fff',
  },
  rowProfile: {
    marginBottom: 5
  }
})

const mapStateToProps = (state) => ({
  profile: state.profile,
});

const mapDispatchToProps = (dispatch) => ({
  getProfile: () => dispatch(ProfileActions.profileRequest()),
  editProfile: (payload) => dispatch(ProfileActions.editProfileRequest(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileScreen);
