import React, {Component} from 'react';
import { View, Text, StyleSheet, ScrollView, TextInput, Picker, TouchableOpacity, Image } from 'react-native';
import {Column as Col, Row} from 'react-native-flexbox-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux';
import ProfileActions from './../../Redux/Profile';


const maskText = (something) => {
  return something ? something : "Empty"
}


class ProfileScreen extends React.Component { 
  constructor(props) {
    super(props);
    this.state = {
      profile: {}
    };
  }

  componentDidMount() {
    this.props.getProfile()
  }

  componentWillReceiveProps(nextProps) {
    const {fetching, error, payload, message} = nextProps.profile;
    if (fetching === false) {
      if(error === false){
        this.setState(payload)
      }else{
        alert(JSON.stringify(message))
      }
    }
  }

  _iconClick = (to, dispatch) => {
    console.log(this.props.navigation.navigate({key: to, routeName: to, params: {...this.state.profile}}));
    this.props.navigation.navigate({key: to, routeName: to, params: {...this.state.profile}});
    if(dispatch !== null){
      this.props[dispatch]();
      console.log(this.props[dispatch]());
    }
  }

  _logoutClick = () => {
    this.props.logout()
  }

  render() {
    const {profile} = this.state;
    let gender = '';
    if (profile.gender == 0) {
      gender = 'Empty'
    } else if (profile.gender == 1) {
      gender = 'Male'
    } else if (profile.gender == 2) {
      gender = 'Female'
    }
    return (
      <View style={styles.container}>
        <View style={styles.header}>
        </View>

        <ScrollView>
        <View alignItems="flex-end" backgroundColor="white" padding={20}>
            <TouchableOpacity onPress={this._logoutClick}>
                <View flexDirection="row" >
                  <Text style={styles.logout}>
                    Logout
                  </Text>
                  </View>
                </TouchableOpacity>
            </View>
          <View style={styles.profileHeader}>
            <Row size={12}>
              <Col sm={3} style={styles.icon}>
                <Image
              // source={{uri: `https://beta.proofn.com/avatar//avatar/32b871c8-c31b-4480-b7df-13547750c97d/6fc271937c9532a8606a68f1dabee9ca725f7cb32d4b879721ed3c7317b84e1b.jpeg`}}
              source={{uri: 'https://cms.kelleybluebookimages.com/content/dam/kbb-editorial/make/lamborghini/lamborghini-other/2019-lambo-urus/01-2019-lamborghini-urus-oem.jpg'}}
                  // source={require('./../../../Assets/images/proofn.png')}
                  style={{width: 100, height: 100, borderRadius: 50, backgroundColor: 'red', resizeMode: 'stretch'}}
                />
              </Col>
              <Col sm={8} style={styles.content}>
                <Text style={styles.textHeader}>Proofn Profile</Text>
                <Text style={styles.textContent}>{maskText(profile.fullName)}</Text>
                <Text>{profile.email}</Text>
              </Col>
              <Col sm={1} style={styles.icon}>
                <TouchableOpacity onPress={this._iconClick.bind(this, 'EditProfile', null)}>
                  <Ionicons name={'ios-build'} size={25} color={'#ffa216'} />
                </TouchableOpacity>
              </Col>
            </Row>
          </View>

          <View style={styles.profile}>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-person'} size={25} color={'#ffa216'}/>
              </Col>
              <Col sm={10}>
                <Text style={styles.textProfile}>{maskText(profile.fullName)}</Text>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-transgender'} size={25} color={'#ffa216'}/>
              </Col>
              <Col sm={10}>
                <Text style={styles.textProfile}>{gender}</Text>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-call'} size={25} color={'#ffa216'}/>
              </Col>
              <Col sm={10}>
                <Text style={styles.textProfile}>{maskText(profile.phoneNumber)}</Text>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-home'} size={25} color={'#ffa216'}/>
              </Col>
              <Col sm={10}>
                <Text style={styles.textProfile}>{maskText(profile.homeAddress)}</Text>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
                <Ionicons name={'ios-briefcase'} size={25} color={'#ffa216'}/>
              </Col>
              <Col sm={10}>
                <Text style={styles.textProfile}>{maskText(profile.office)}</Text>
              </Col>
            </Row>
            <Row size={12} style={styles.rowProfile}>
              <Col sm={2} style={styles.icon}>
              </Col>
              <Col sm={10}>
                <Text style={styles.textProfile}>{maskText(profile.profession)}</Text>
              </Col>
            </Row>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'orange',
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
    textAlign: 'left'
  },
  logout: {
    alignItems: 'center',
    justifyContent: 'center',
    color: 'orange',
  },
  profileHeader: {
    padding: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    paddingLeft: 10,
    justifyContent: 'center',
  },
  textHeader: {
    color: '#ffa216',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10
  },
  textContent: {
    fontWeight: 'bold'
  },
  profile: {
    padding: 10,
    marginTop: 20,
    backgroundColor: '#fff',
  },
  rowProfile: {
    padding: 10,
  },
  textProfile: {
    alignItems: 'center',
    justifyContent: 'center',
  }
})

const mapStateToProps = (state) => ({
  profile: state.profile,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch({ type: 'LOGOUT' }),
  getProfile: () => dispatch(ProfileActions.profileRequest())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
