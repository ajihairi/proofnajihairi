
import React, {Component} from 'react'
import {Provider} from 'react-redux'
import RootContainer from './RootContainer'
import createStore from '../Redux'
import {PersistGate} from 'redux-persist/lib/integration/react'
// create our store
export const {persistor, store, networkStart} = createStore()


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      store: networkStart(() => this.setState({isLoading: false})),
    }
  }

  componentWillMount() {

  }

  render() {
    if (this.state.isLoading) return null;

    return (
      <PersistGate persistor={persistor}>
        <Provider store={store}>
          <RootContainer/>
        </Provider>
      </PersistGate>
    )
  }
}

  export default App
