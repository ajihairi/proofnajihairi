import React, { Component } from 'react'
import { View, StatusBar, Text } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
// import AppActions from '../Redux/App'
import {StyleSheet} from 'react-native'

// Styles


class RootContainer extends Component {
  componentDidMount () {
    this.props.startup();
    // this.props.location();
  }

  render () {
    return (
      <View style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        <ReduxNavigation />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  applicationView: {
    flex: 1
  }
});


// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup()),
  // location: () => dispatch(AppActions.locationChange())
});

export default connect(null, mapDispatchToProps)(RootContainer)
