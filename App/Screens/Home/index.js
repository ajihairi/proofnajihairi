import React, {Component} from 'react';
import { View, Button, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TabNavigator, TabBarBottom } from 'react-navigation';
import InboxScreen from './../Inbox';
import ProfileScreen from './../Profile/profile';
import ContactScreen from '../Contact';
import GroupScreen from '../Groups';


export default TabNavigator(
  {
    Inbox: { screen: InboxScreen },
    Contact:{screen: ContactScreen},
    Group: {screen: GroupScreen},
    Profile: { screen: ProfileScreen },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Inbox') {
          iconName = `ios-mail${focused ? '' : '-outline'}`;
        } else if (routeName === 'Profile') {
          iconName = `ios-person${focused ? '' : '-outline'}`;
        } else if (routeName == 'Contact') {
          iconName = `ios-contact${focused ? '' : '-outline'}`;
        } else if (routeName == 'Group') {
          iconName = `ios-contacts${focused ? '' : '-outline'}`;
        }

        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: '#ffa216',
      inactiveTintColor: 'gray',
    },
    animationEnabled: true,
    swipeEnabled: false,
  }
);