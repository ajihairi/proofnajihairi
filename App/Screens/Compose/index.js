import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Alert,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import SendMessageActions from './../../Redux/Compose';
import { Header, Text, Left, Body, Right, Icon, Title, Button } from 'native-base';
import styling from '../../Style/Style';
const avatar = require('../../Assets/icons/avatar.png');
const sendImage = require('../../Assets/icons/sendIcon.png');
const bgCompose = require('../../Assets/images/chatbg.png');
const emojiw = require('../../Assets/icons/emoji.png');
import { Card, CardSection, Input } from '../../Components/common';
import MessageActions from './../../Redux/Inbox';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkStyleSheet,
  RkTheme
} from 'react-native-ui-kitten';

class ComposeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: {
        subject: null,
        content: null,
        recipients: null,
        isfilled: false
      }
    };
  }
  _cancelClick = () => {
    Alert.alert(
      '',
      'Do You Want to Discard the Email?',
      [
        { text: 'Yes', onPress: () => this.props.navigation.goBack() },
        { text: 'No', onPress: () => console.tron.log('No'), style: 'cancel' }
      ],
      { cancelable: false }
    )
  }
  componentWillReceiveProps(nextProps) {
    const { fetching, error, message } = nextProps.sendMessage;
    if (fetching === false) {
      if (error === false) {
        Alert.alert('Success Send Message');
        this.setState({
          message: {
            subject: null,
            content: null,
            recipients: null
          }
        })
        this.props.navigation.goBack();
        this.props.getMessage();
      } else {
        alert('Failure Send Message');
      }
    }
  }

  _inputMessage = (key, value) => {
    let message = Object.assign({}, this.state.message);
    message[key] = value;
    this.setState({ message });
    console.tron.log(this.state)
  }

  _sendClick = () => {
    const message = Object.assign({}, this.state.message);
    const { recipients } = this.state.message
      if (recipients) {
        const recipientsEmail = [{
          email: recipients
        }]
        message.recipients = recipientsEmail
        this.props.postSendMessage(message);
      } else {
        Alert.alert('Please Insert Email!');
      }
    } 

  render() {
    return (
      <View style={styles.container}>
        <View style={styling.header}>
          <Left style={{ flex: 1 }}>
            <TouchableOpacity onPress={this._cancelClick} style={{ paddingLeft: 10 }}>
              <Ionicons name={'ios-close'} size={50} color={'#fff'} />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 2 }}>
            <Text style={styling.headerText}>Compose Message</Text>
          </Body>
          <Right style={{ flex: 1, marginRight: 5 }}>
          </Right>
        </View>
        {/*================for composing message===========*/}
        <View style={{ flex: 1, backgroundColor: 'gray' }}>
          <ImageBackground source={bgCompose} style={{ padding: 10, flex: 1, width: null, height: null }}>
            <View>
              <View style={styles.textinputcontainer}>
                <Text note>To : </Text>
                <TextInput
                  autoCapitalize="none"
                  value={this.state.message.recipients}
                  placeholder={'Insert Reciepent Email'}
                  style={styles.textEmailInput}
                  underlineColorAndroid='transparent'
                  onChangeText={this._inputMessage.bind(this, 'recipients')}
                />
              </View>

              <View style={styles.textinputcontainer}>
                <Text note>Subject : </Text>
                <TextInput
                  value={this.state.message.subject}
                  placeholder={'New Topic'}
                  style={styles.textEmailInput}
                  underlineColorAndroid='transparent'
                  onChangeText={this._inputMessage.bind(this, 'subject')}
                />
              </View>

              <View style={styles.messagecontainer}>
                <ScrollView>
                  <TextInput
                    value={this.state.message.content}
                    placeholder={'Write Message'}
                    style={styles.textEmailInput}
                    underlineColorAndroid='transparent'
                    multiline={true}
                    numberOfLines={5}
                    onChangeText={this._inputMessage.bind(this, 'content')}
                  />
                </ScrollView>
              </View>

              <View flexDirection="row" justifyContent="space-between">
                <TouchableOpacity style={styles.buttonContainers2}>
                  <Image source={emojiw} resizeMode="contain" style={{ width: 30, height: 30 }} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonContainers}>
                  <Icon name="ios-camera" style={styles.iconstyle1} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonContainers}>
                  <Icon name="ios-attach" style={styles.iconstyle2} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.buttonContainers}>
                  <Icon name="ios-add" style={styles.iconstyle3} />
                </TouchableOpacity>
              </View>
              <Button block style={styles.sendButton} onPress={this._sendClick}
              >
                <Text style={{ fontWeight: 'bold' }}>Send Message</Text>
              </Button>
            </View>

          </ImageBackground>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  sendButton: {
    marginVertical: 20,
    padding: 20,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#2ecc71'
  },
  buttonContainers: {
    borderRadius: 50,
    backgroundColor: 'white',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'orange',
    borderWidth: 1
  },

  buttonContainers2: {
    borderRadius: 50,
    backgroundColor: 'orange',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'orange',
    borderWidth: 1
  },
  iconstyle1: {
    color: 'red',
    fontSize: 35,
    fontWeight: 'bold'
  },
  iconstyle2: {
    color: '#2ecc71',
    fontSize: 35,
    fontWeight: 'bold'
  },
  iconstyle3: {
    color: 'blue',
    fontSize: 35,
    fontWeight: 'bold'
  },
  textinputcontainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderColor: 'orange',
    borderWidth: 1,
    flexDirection: 'row',
    marginVertical: 10,
    height: 30
  },

  messagecontainer: {
    backgroundColor: 'white',
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderColor: 'orange',
    borderWidth: 1,
    flexDirection: 'row',
    marginVertical: 10,
    height: 250
  },
  header: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#ffa216',
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
    textAlign: 'left'
  },
  sendCompase: {
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
  },
  content: {
    padding: 20
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#a0a0a0',
  },
  iconEmailInput: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textEmailInput: {
    borderColor: '#fff'
  },
  border: {
    borderWidth: 1,
    borderColor: 'black',
  },
  contentSubject: {
    paddingTop: 5,
    paddingRight: 20,
    paddingLeft: 20,
    paddingBottom: 5
  }
})


const mapStateToProps = (state) => (console.tron.log(state), {
  sendMessage: state.sendMessage,
});

const mapDispatchToProps = (dispatch) => ({
  getMessage: () => dispatch(MessageActions.messageRequest()),
  postSendMessage: (payload) => dispatch(SendMessageActions.sendMessageRequest(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(ComposeScreen);

// export default ComposeScreen