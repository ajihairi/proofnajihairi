import React, { Component } from 'react';
import {
  View, StyleSheet, Image, TouchableOpacity, ImageBackground,
  KeyboardAvoidingView,
  ScrollView, Alert, AsyncStorage
} from 'react-native';
import { Column as Col, Row } from 'react-native-flexbox-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  List,
  ListItem,
  Text,
  Thumbnail,
  Input,
  Left,
  Body,
  Right,
  Footer,
  StyleProvider,
  Subtitle
} from "native-base";
import MessageDetailActions from './../../Redux/Inbox/messageDetail';
import DeleteMessageActions from './../../Redux/Inbox/deleteMessage';
import MessageActions from './../../Redux/Inbox'
import { create } from 'apisauce';
import jwtDecode from 'jwt-decode';

const bgCompose = require('../../Assets/images/chatbg.png');
var styling = require('../../Style/Style');
const avatar = require('../../Assets/icons/avatar.png');
const defaultAvatar1 = require("../../Assets/icons/usericon.png");
const emoji = require("../../Assets/icons/emoji.png");
const send = require('../../Assets/icons/send.png');
const api = create({
  baseURL: 'https://beta.proofn.com/v1/',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
});

class MessageDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    const params = this.props.navigation.state.params;
    this.state = {
      message: null,
      idHash: params,
      inputreply: "",
      data: []
      

    };
    this.delete = false;
  }
  sendButton() {
    Alert.alert("tombol send di pencet");
    this.setState({inputreply: ""});
  }
  bottomMenu() {
    Alert.alert("bottom Menu di pencet")
  }
  _inputReply(){
    const inputreply = this.state.inputreply;
    this.setState({ inputreply })
    console.log(inputreply)
  }

  componentDidMount() {
    this.props.getMessageDetail(this.state.idHash)
    console.log("id Hash", this.state.idHash);

  }

  componentWillReceiveProps(nextProps) {

  }

  _backClick = () => {
    this.props.navigation.goBack()
  }
  _deleteMessage = () => {
    const { id, idHash } = this.state;
    Alert.alert(
      '',
      'Do You Want to Delete this Email?',
      [
        {
          text: 'Yes', onPress: () => {
            AsyncStorage.getItem('token')
              .then((token) => {
                const user = jwtDecode(token);
                console.log('token', token);
                console.log(idHash);
                // console.log('https://beta.proofn.com/v1/messages/inbox/'.concat(this.state.idHash,'/trash'));
                fetch('https://beta.proofn.com/v1/messages/inbox/'.concat(idHash, '/trash'), {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                  },
                }).then((response) => response.json())
                  .then((responseJson) => {
                    Alert.alert(responseJson.message);
                    this.props.navigation.goBack(null);
                    this.props.getMessage();

                  }).catch((error) => {
                    console.error(error);
                  });
              });
          }
        },
        { text: 'No', onPress: () => console.tron.log('No'), style: 'cancel' }
      ],
      { cancelable: false }
    )

  }

  // _listMessageRecive = () => {
  //   const { data } = this.props.messageDetail.payload
  //   console.log(data)
  //   if (data == null) {
  //     return (
  //       <View>
  //         <Text> No Data</Text>
  //       </View>
  //     )
  //   }
  //     else {
  //   // const lists = this.state.data;
  //   // console.log(this.state.data);
  //   // const datadetail = lists.map((datadetail, i) => (
  //       <TouchableOpacity 
  //       // key={i}
  //       onLongPress={this._deleteMessage}
  //       delayLongPress={1000}
  //       >
  //       <View style={styles.bubbleinbox}>
  //       <View style={styles.contentHeader}>
  //             <Row size={10}>
  //               <Col sm={2} style={styles.icon}>
  //                 <Image source={avatar} 
  //                 style={{
  //                   width: 50, 
  //                   height: 50,
  //                   resizeMode: 'stretch',
  //                   backgroundColor: 'white'}}/>
  //               </Col>
  //               <Col sm={8} style={styles.colEmail}>
  //                 <Text style={styles.textEmail}>{data.sender.fullName}</Text>
  //                 <Text>{data.sender.activeEmail}</Text>
  //               </Col>
  //             </Row>
  //           </View>
  //           <View style={styles.content}>
  //             <View style={styles.contentSubject}>
  //               <Text style={styles.textSubject}>{data.subject}</Text>
  //             </View>
  //             <View style={styles.contentDateTime}>
  //               <Text style={styles.textDateTime}>{data.sentAt}</Text>
  //             </View>
  //             <View style={styles.contentText}>
  //               <Text>{data.content}</Text>
  //             </View>
  //           </View>
  //           </View>
  //       </TouchableOpacity>
  //   // ))
  //   // return datadetail
  // }

  // }
  render() {
    const { data } = this.props.messageDetail.payload
    console.log(data)
    if (data == null) {
      return (
        <View>
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <View style={styling.header}>
            <Left style={{ flex: 1 }}>
              <TouchableOpacity onPress={this._backClick} style={{ paddingLeft: 10 }}>
                <Ionicons name={'ios-arrow-back'} size={25} color={'#fff'} />
              </TouchableOpacity>
            </Left>
            <Body style={{ flex: 3 }}>
              <Text style={styling.headerText}>{data.sender.fullName}</Text>
              <Text style={{ color: 'white', fontSize: 12 }}>{data.sender.activeEmail}</Text>
            </Body>
            <Right style={{ flex: 1 }}>
              <View flexDirection="row" >
                <TouchableOpacity onPress={this._deleteMessage}>
                  <Icon name="ios-trash"
                    style={{ marginHorizontal: 10, fontSize: 20, color: 'white' }} />
                </TouchableOpacity>
              </View>
            </Right>
          </View>

          <View flex={1}>
            <ImageBackground
              source={bgCompose} style={{ flex: 1, width: null, height: null }}>
              <ScrollView style={{ padding: 3 }}>

                <TouchableOpacity
                  // key={i}
                  onLongPress={this._deleteMessage}
                  delayLongPress={1000}
                  style={styles.bubbleinbox}
                >
                  <View>
                    <View>

                    </View>
                    <View style={styles.contentHeader}>
                      <Row size={10}>
                        <Col sm={2} style={styles.icon}>
                          <Image source={avatar}
                            style={{
                              width: 50,
                              height: 50,
                              resizeMode: 'stretch',
                              backgroundColor: 'white'
                            }} />
                        </Col>
                        <Col sm={8} style={styles.colEmail}>
                          <Text style={styles.textEmail}>{data.subject}</Text>
                        </Col>
                      </Row>
                    </View>
                    <View style={styles.content}>
                      <View style={styles.contentDateTime}>
                        <Text style={styles.textDateTime}>{data.sentAt}</Text>
                      </View>
                      <View style={styles.contentText}>
                        <Text>{data.content}</Text>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>

                {/* {this._listMessageRecive()} */}
              </ScrollView>

              <KeyboardAvoidingView
                behavior='padding'
                justifyContent='flex-end'>
                <Footer
                  style={bottomStyle.footer}>
                  <View
                    style={bottomStyle.viewfooter}>
                    <View flexDirection="row" style={bottomStyle.inputBox}>
                      <Input placeholder="write message.." 
                      style={bottomStyle.inputstyle}
                      onChangeText={this._inputReply.bind(this)}/>
                      <TouchableOpacity 
                      onPress={this.sendButton.bind(this)} 
                      style={bottomStyle.sendbuttons} >
                      <Image source={send} resizeMode="contain" 
                      style={{ 
                        width: 30, 
                        height: 30,
                        marginHorizontal: 10 }} />
                      </TouchableOpacity>
                    </View>
                    <View flexDirection="row" style={bottomStyle.containerbottom}>
                      <Button style={bottomStyle.buttons} onPress={this.bottomMenu} ><Image source={emoji} resizeMode="contain" style={{ width: 20, height: 20 }} /></Button>
                      <Button style={bottomStyle.buttons} onPress={this.bottomMenu}><Icon name="camera" /></Button>
                      <Button style={bottomStyle.buttons} onPress={this.bottomMenu}><Icon name="attach" /></Button>
                      <Button style={bottomStyle.buttons} onPress={this.bottomMenu}><Icon name="paper" /></Button>
                    </View>
                  </View>
                </Footer>
              </KeyboardAvoidingView>
            </ImageBackground>
          </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  bubbleinbox: {
    flex: 1,
    marginHorizontal: 5,
    marginVertical: 15,
    marginRight: 50,
    borderRadius: 10,
    backgroundColor: 'white',
    shadowColor: 'rgba(99, 110, 114,1.0)',
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 10
  },
  header: {
    padding: 10,
    backgroundColor: '#ffa216',
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#fff',
  },
  iconHeader: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentHeader: {
    // flex: 1,
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#d6d6d6',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  colEmail: {
    paddingLeft: 10,
    justifyContent: 'center',
  },
  textEmail: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  content: {
    padding: 5,
    paddingHorizontal: 10,
  },
  textSubject: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  contentDateTime: {

  },
  textDateTime: {
    fontSize: 11,
    color: '#adadad'
  },
  contentText: {
    paddingBottom: 10
  }
});

const bottomStyle = StyleSheet.create({
  sendbuttons: {
    marginTop: 5,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  buttons: {
    flex: 1,
    borderRadius: 100,
    height: 40,
    width: 70,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  inputBox: {
    flex: 1,
    backgroundColor: 'orange',
    alignItems: 'flex-start',
    justifyContent: 'center',
    height: 40
  },
  inputstyle: {
    flex: 1,
    height: 30,
    backgroundColor: 'white',
    marginLeft: 5,
    borderRadius: 25,
    paddingLeft: 15,
    paddingRight: 5,
    marginTop: 5
  },
  containerbottom: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    backgroundColor: 'orange',
    height: 40,
  },
  viewfooter: {
    flex: 1,
    justifyContent: 'flex-end',
    borderTopColor: 'gray',
    borderTopWidth: 1,
  },
  footer: {
    justifyContent: 'flex-end',
    flexDirection: 'column',
    backgroundColor: 'white',
    height: 100
  },

  //header
  headerleft:
  {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  containertitle: {
    flexDirection: 'column'
  },
  titlestyle: {
    fontSize: 12, color: 'white'
  },
  subtitlestyle: {
    fontSize: 10, color: 'white'
  },
  iconheaderstyle: {
    marginHorizontal: 10,
    fontSize: 18,
    color: 'white',

  },
  backbuttons: {
    color: 'white'
  },
});


const mapStateToProps = (state) => {
  console.tron.log(state)
  return {
    messageDetail: state.messageDetail,
  }
};

const mapDispatchToProps = (dispatch) => ({
  getMessage: () => dispatch(MessageActions.messageRequest()),
  getMessageDetail: (payload) => dispatch(MessageDetailActions.messageDetailRequest(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(MessageDetailScreen);
