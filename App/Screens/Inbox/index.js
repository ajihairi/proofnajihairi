import React, { Component } from 'react';
import { View, Text, StyleSheet,Alert, TouchableHighlight, ScrollView, TouchableOpacity, CheckBox, Image } from 'react-native';
import { connect } from 'react-redux';
import MessageActions from './../../Redux/Inbox'
import { Column as Col, Row } from 'react-native-flexbox-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Header, Left, Body, Right, Icon, Title } from 'native-base';
import DeleteMessageActions from './../../Redux/Inbox/deleteMessage';

import styling from '../../Style/Style';
const avatar = require('../../Assets/icons/avatar.png');

class InboxScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      
    };
    this.delete = false;
    this.selected = true;
  }

  componentDidMount() {
    this.props.getMessage()
  }

  componentWillReceiveProps(nextProps) {
    const { fetching, error, payload, message } = nextProps.message;
    if (fetching === false) {
      if (error === false) {
        const newItem = payload.data.map((x, i) => {
          const dateTime = x.sentAt;
          const date = dateTime.split('T')
          x.dateSend = date[0]
          x.checked = false
          return x
        })
        this.setState({
          data: newItem
        })
      } else {
        alert(JSON.stringify(message))
      }
    }

    if (fetching === false && this.delete) {
      if (error === false) {
        alert(JSON.stringify('Success Delete'));
        this.props.getMessage()
        this.delete = false;
      } else {
        alert(JSON.stringify('Failure Delete'))
      }
    }
  }

  _messageClick = (to, dispatch, idHash) => {
    this.props.navigation.navigate({ key: to, routeName: to, params: idHash });
    if (dispatch !== null) {
      this.props[dispatch]();
    }
  }

  _messageLongClick = (idHash) => {
    Alert.alert("Id+Hash ", idHash);
    // const data = {
    //   idHash: idHash,
    //   backgroundColor: '#d3e4ff'
    // }
    // this.state.selected.push(data)
  }

  _checkBox = (idHash, index) => {
    const data = this.state.data
    data[index].checked = !data[index].checked
    this.setState({
      data
    })
  }

  _deleteMessage = () => {
    const list = this.state.data
    list.map((data, i) => {
      if (data.checked == true) {
        this.props.deleteMessage(data.id + '-' + data.hash)
      }
    })
    this.delete = true
  }

  _composeMessage = (to, dispatch) => {
    console.log(this.props.navigation.navigate({ key: to, routeName: to}));
    this.props.navigation.navigate({ key: to, routeName: to});
    if (dispatch !== null) {
      this.props[dispatch]();
    }
  }

  _listMessage = () => {
    const lists = this.state.data;
    const data = lists.map((data, i) => (
      <TouchableOpacity
        key={i}
        onPress={this._messageClick.bind(this, 'MessageDetail', null, data.id + '-' + data.hash)}
        onLongPress={this._messageLongClick.bind(this, data.id + '-' + data.hash)}
        delayLongPress={1000}
      >
        <Row size={12} style={styles.row}>
          <Col sm={3} style={styles.icon}>
            <Image source={avatar} style={{ resizeMode: 'contain', width: 50, height: 50 }} />
          </Col>
          <Col sm={7}>
            <Text style={styles.textSender} numberOfLines={1}>{data.sender.activeEmail}</Text>
            <Text style={styles.textSubject} numberOfLines={1}>{data.subjectPreview}</Text>
            <Text style={styles.textContent} numberOfLines={1}>{data.contentPreview}</Text>
          </Col>
          <Col sm={2}>
            <Text style={styles.time}>{data.dateSend}</Text>
            {/* <CheckBox style={{width: 15, height: 15}}
              onChange={this._checkBox.bind(this, data.id + '-' + data.hash, i)}
              value={this.state.data[i].checked}
            /> */}
          </Col>
        </Row>
      </TouchableOpacity>
    ))
    return data
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styling.header}>
          <Left style={{ flex: 1 }}>
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={styling.headerText}>Inbox</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <View flexDirection="row" >
              <TouchableOpacity onPress={this._composeMessage.bind(this, 'ComposeScreen', null)}>
                <Icon name="ios-create"
                  style={{ marginHorizontal: 5, fontSize: 20, color: 'white' }} />
              </TouchableOpacity>
            </View>
          </Right>
        </View>

        <ScrollView>
          {this._listMessage()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    flexDirection: 'row',
    paddingVertical: 25,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffa216',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    alignSelf: 'center'

  },
  row: {
    paddingTop: 10,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#a0a0a0',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textSender: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  textSubject: {
    fontSize: 12,
  },
  textContent: {
    fontSize: 11,
    color: '#a0a0a0'
  },
  time: {
    fontSize: 9,
    color: '#ffa216'
  }
})

const mapStateToProps = (state) => ({
  message: state.message,
  deleteMessage: state.deleteMessage
});

const mapDispatchToProps = (dispatch) => ({
  getMessage: () => dispatch(MessageActions.messageRequest()),
  deleteMessage: (idx) => dispatch(DeleteMessageActions.deleteMessageRequest(idx)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InboxScreen);