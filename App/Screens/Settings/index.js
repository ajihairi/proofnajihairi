import React, {Component} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import {
  RkText,
  RkStyleSheet,
  RkTheme
} from 'react-native-ui-kitten';
import Icon from 'react-native-vector-icons/FontAwesome';
import {connect} from 'react-redux'
import LoginActions from './../../Redux/Auth/LoginRedux'
import {NavigationActions} from 'react-navigation'

class SettingsScreen extends Component{
  static navigationOptions = {
    title: 'SETTINGS',
  };

  constructor(props){
    super(props)
  }

  _logoutClick = () => {
    this.props.logout()
  };

  render(){
    return(
      <ScrollView style={styles.container}>
        <View style={styles.section}>
          <View style={styles.row}>
            <TouchableOpacity onPress={this._logoutClick} style={styles.rowButton}>
              <Icon name={'sign-out'} size={25} style={{marginRight: 10}} />
              <RkText rkType='header6'>Logout</RkText>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    )
  }
}

let styles = RkStyleSheet.create(theme => ({
  container: {
    backgroundColor: theme.colors.screen.base,
  },
  header: {
    paddingVertical: 25
  },
  section: {
    marginVertical: 25
  },
  heading: {
    paddingBottom: 12.5
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 17.5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border.base,
    alignItems: 'center'
  },
  rowButton: {
    flex: 1,
    paddingVertical: 24,
    flexDirection: 'row'
  },
  switch: {
    marginVertical: 14
  },
}));

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch({ type: 'LOGOUT' }),
});

export default connect(null, mapDispatchToProps)(SettingsScreen)
