import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';


class LoadingScreen extends React.Component {
  render() {
    return (
      <View style={styles.screen}>
        <MaterialIndicator color='#8f70dd'/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  }
});

export default LoadingScreen
