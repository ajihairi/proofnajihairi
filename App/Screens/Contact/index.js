import React, { Component } from "react";
import { StyleSheet, Alert, View, TouchableOpacity } from 'react-native'
import {
    Container,
    Header,
    Title,
    Content,
    Button,
    Icon,
    List,
    ListItem,
    Text,
    Thumbnail,
    Left,
    Body,
    Right,
    StyleProvider
} from "native-base";


var styling = require('../../Style/Style');
const avatar = require('../../Assets/icons/avatar.png');
const defaultAvatar1 = require("../../Assets/icons/usericon.png");

const datas = [
    {
        img: defaultAvatar1,
        text: "Aceng Pilek",
        note: "acengpilek@proofn.com"
    },
    {
        img: defaultAvatar1,
        text: "Dadang Warnet",
        note: "dadangwarnet@proofn.com"
    },
    {
        img: defaultAvatar1,
        text: "Kopi Joni",
        note: "kopijoni@proofn.com"
    },
    {
        img: defaultAvatar1,
        text: "Hotman Paris",
        note: "parissultan@proofn.com"
    },
    {
        img: defaultAvatar1,
        text: "Bocil Ganggu",
        note: "sabdabocil@proofn.com"
    }
];

class ContactScreen extends Component {

    _addContact(){
        Alert.alert("add contact button pressed")
    }
    _searchContact(){
        Alert.alert("search contact pressed")
    }
    render() {
        return (
            <Container style={styles.container}>
                    <View style={styling.header}>
                        <Left style={{ flex: 1 }}>
                        </Left>
                        <Body style={{ flex: 1 }}>
                            <Title style={styling.headerText}>Contact</Title>
                        </Body>
                        <Right style={{ flex: 1 }}>
                            <View flexDirection="row" >
                                <TouchableOpacity onPress={this._searchContact}>
                                    <Icon name="ios-search"
                                        style={{ marginHorizontal: 10, fontSize: 20, color: 'white' }} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this._addContact}>
                                    <Icon name="md-add"
                                        style={{ marginHorizontal: 5, fontSize: 20, color: 'white' }} />
                                </TouchableOpacity>
                            </View>
                        </Right>
                </View>

                <Content>
                    <List >
                        {datas.map((data, i) => (
                            <ListItem avatar key={i} onPress={() => Alert.alert(data.note)}>
                                <Left>
                                    <Thumbnail small source={data.img} />
                                </Left>
                                <Body>
                                    <Text>{data.text}</Text>
                                    <Text numberOfLines={1} note>
                                        {data.note}
                                    </Text>
                                </Body>
                            </ListItem>
                        ))}
                    </List>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#FFF"
    },
});

export default ContactScreen;