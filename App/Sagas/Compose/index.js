import { call, put } from 'redux-saga/effects'
import SendMessageActions from '../../Redux/Compose'

export const postSendMessageSaga = function * (api, action) {
  try {
    const { body } = action;
    const response = yield call(api.postSendMessage, body);
    if (response.ok) {
      if (response){
        const message = response;
        yield put(SendMessageActions.sendMessageSuccess(message));
      } else {
        const message = response;
        yield put(SendMessageActions.sendMessageFailure(message));
      }
    } else {
      const message = response.problem;
      yield put(SendMessageActions.sendMessageFailure(message));
    }
  }catch (err){
    const message = 'error tidak diketahui, periksa kembali jaringan anda.';
    yield put(SendMessageActions.sendMessageFailure(message));
  }

};
