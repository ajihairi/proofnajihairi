import { call, put } from 'redux-saga/effects'
import ProfileActions from '../../Redux/Profile'

export const getProfileSaga = function * (api, action) {
  try {
    const response = yield call(api.getProfile);
    if (response.ok) {
      if (response.data){
        const payload = {
          profile: response.data
        };
        const message = response.data;
        yield put(ProfileActions.profileSuccess(payload, message));
      } else {
        const message = response.data;
        yield put(ProfileActions.profileFailure(message));
      }
    } else {
      const message = response.problem;
      yield put(ProfileActions.profileFailure(message));
    }
  }catch (err){
    const message = err;
    yield put(ProfileActions.profileFailure(message));
  }

};

export const editProfileSaga = function * (api, action) {
  try {
    const { body } = action;
    const response = yield call(api.putProfile, body);
    if (response.ok) {
      if (response){
        const message = response;
        yield put(ProfileActions.profileSuccess(message));
      } else {
        const message = response;
        yield put(ProfileActions.profileFailure(message));
      }
    } else {
      const message = response.problem;
      yield put(ProfileActions.profileFailure(message));
    }
  }catch (err){
    const message = err;
    yield put(ProfileActions.profileFailure(message));
  }

};
