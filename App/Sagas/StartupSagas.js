import { put, select } from 'redux-saga/effects'
import LoggedInActions, { isLoggedIn } from '../Redux/Auth/LoginRedux'
// process STARTUP actions

export const selectLoggedInStatus = (state) => {
  return isLoggedIn(state.auth.token)
};

export const selectRoleStatus = (state) => {
  return state.auth.role
};


export function * startup () {
  const isLoggedIn = yield select(selectLoggedInStatus);
  if (isLoggedIn) {
    const role = yield select(selectRoleStatus);
    yield put(LoggedInActions.autoLogin(role))
  }else{
    yield put(LoggedInActions.logout())
  }
}
