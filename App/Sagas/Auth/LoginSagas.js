import { call, put } from 'redux-saga/effects'
import LoggedInActions from '../../Redux/Auth/LoginRedux'
import {api as apiTampan} from './../index'
import {AsyncStorage} from 'react-native'

export const loginSaga = function * (api, action) {
  try {
    const { identifier, password } = action;
    const response = yield call(api.postLogin, { identifier, password });
    if (response.ok) {
      if (response.data){
        const { token } = response.data;
        const token_string = `${token}`;
        const { api } = apiTampan;
        api.setHeader('Authorization', `Bearer ${token}`);
        yield put(LoggedInActions.loginSuccess(token_string));
        yield put(LoggedInActions.autoLogin('admin'))
        AsyncStorage.setItem('token', token)
        .then(() => {
          console.log('Login success!');
          console.log('token', token);
        })
        .catch((err) => { // Gagal menyimpan ke AsyncStorage
          console.log('Login failed', err);
        })
        .done();
      } else {
        const message = response.message
        yield put(LoggedInActions.loginFailure(message));
      }
    } else {
      const message = response.problem;
      yield put(LoggedInActions.loginFailure(message));
    }
  }catch (err){
    const message = 'Galat tidak diketahui, periksa kembali jaringan anda.';
    yield put(LoggedInActions.loginFailure(message));
  }

};
