import { call, put } from 'redux-saga/effects'
import MessageActions from '../../Redux/Inbox/deleteMessage'
// import queryString from  'query-string';

export const deleteMessageSaga = function * (api, action) {
  try {
    const {idx} = action;
    const response = yield call(api.deleteMessage, idx);
    if (response.ok) {
      if (response.message){
        const message = response.message;
        yield put(MessageActions.deleteMessageSuccess(message));
      } else {
        const message = response.message;
        yield put(MessageActions.deleteMessageFailure(message));
      }
    } else {
      const message = response.message;
      yield put(MessageActions.deleteMessageFailure(message));
    }
  }catch (err){
    const message = 'Galat tidak diketahui, periksa kembali jaringan anda.';
    yield put(MessageActions.deleteMessageFailure(message));
  }

};
