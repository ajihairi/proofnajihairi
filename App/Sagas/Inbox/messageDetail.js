import { call, put } from 'redux-saga/effects'
import MessageDetailActions from './../../Redux/Inbox/messageDetail'

export const getMessageDetailSaga = function * (api, action) {
  try {
    console.tron.log('kimak')
    const { body } = action
    const response = yield call(api.getMessageDetail, body);
    if (response.ok) {
      if (response.data){
        const payload = {
          data: response.data
        };
        const message = response.data;
        yield put(MessageDetailActions.messageDetailSuccess(payload, message));
      } else {
        const message = response.data;
        yield put(MessageDetailActions.messageDetailFailure(message));
      }
    } else {
      const message = response.problem;
      yield put(MessageDetailActions.messageDetailFailure(message));
    }
  }catch (err){
    const message = 'Galat tidak diketahui, periksa kembali jaringan anda.';
    yield put(MessageDetailActions.messageDetailFailure(message));
  }

};
