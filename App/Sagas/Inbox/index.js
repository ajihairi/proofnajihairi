import { call, put } from 'redux-saga/effects'
import MessageActions from './../../Redux/Inbox'

export const getMessageSaga = function * (api, action) {
  try {
    const response = yield call(api.getMessage);
    if (response.ok) {
      if (response.data){
        const payload = {
          data: response.data
        };
        const message = response.data;
        yield put(MessageActions.messageSuccess(payload, message));
      } else {
        const message = response.data;
        yield put(MessageActions.messageFailure(message));
      }
    } else {
      const message = response.problem;
      yield put(MessageActions.messageFailure(message));
    }
  }catch (err){
    const message = err;
    yield put(MessageActions.messageFailure(message));
  }

};
