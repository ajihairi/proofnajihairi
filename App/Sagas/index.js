import { takeLatest, all, fork, takeEvery } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'
import { networkEventsListenerSaga, offlineActionTypes } from 'react-native-offline';

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { LoginTypes } from '../Redux/Auth/LoginRedux'



/* --------------------- Proofn ---------------------- */

import {messageTypes} from '../Redux/Inbox';
import {messageDetailTypes} from '../Redux/Inbox/messageDetail';
import {ProfileTypes} from "../Redux/Profile";
import {SendMessageTypes} from "../Redux/Compose";
import {DeleteMessageTypes} from "../Redux/Inbox/deleteMessage";

/* --------------------- Proofn ---------------------- */




/* ------------- Sagas ------------- */

import { startup } from './StartupSagas';
import {loginSaga} from "./Auth/LoginSagas";
import {logoutSaga} from "./Auth/LogoutSagas";

/* ------------- Sagas ------------- */


// import {openLocationWatch, getUserLocation} from './App/Location';

/* --------------------- Proofn ---------------------- */

import {getMessageSaga} from './Inbox';
import {getMessageDetailSaga} from './Inbox/messageDetail';
import {getProfileSaga, editProfileSaga} from "./Profile";
import {postSendMessageSaga} from './Compose';
import {deleteMessageSaga} from './Inbox/deleteMessage';

/* --------------------- Proofn ---------------------- */



/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
export const api = DebugConfig.useFixtures ? FixtureAPI : API.create();

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    fork(networkEventsListenerSaga, {
      timeout: 2000,
      checkConnectionInterval: 0,
      withRedux: true,
      pingServerUrl: 'https://www.google.com'
    }),

    takeLatest(StartupTypes.STARTUP, startup),
    // fork(openLocationWatch),


    // takeLatest(AppTypes.GET_LOCATION_REQUEST, getUserLocation),


    /* --------------------- Proofn ---------------------- */

    takeLatest(LoginTypes.LOGIN_REQUEST, loginSaga, api),
    takeLatest(LoginTypes.LOGOUT, logoutSaga),

    takeLatest(messageTypes.MESSAGE_REQUEST, getMessageSaga, api),
    takeLatest(messageDetailTypes.MESSAGE_DETAIL_REQUEST, getMessageDetailSaga, api),

    takeLatest(ProfileTypes.PROFILE_REQUEST, getProfileSaga, api),
    takeLatest(ProfileTypes.EDIT_PROFILE_REQUEST, editProfileSaga, api),

    takeLatest(SendMessageTypes.SEND_MESSAGE_REQUEST, postSendMessageSaga, api),

    takeLatest(DeleteMessageTypes.DELETE_MESSAGE_REQUEST, deleteMessageSaga, api)

    /* --------------------- Proofn ---------------------- */

    
  ])
}
