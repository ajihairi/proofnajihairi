import { createActions } from 'reduxsauce'

//action creator, types creator

const { Types, Creators } = createActions({
  startup: null
})

export const StartupTypes = Types
export default Creators
