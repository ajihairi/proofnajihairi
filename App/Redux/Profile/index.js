import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  profileRequest: null,
  profileSuccess: ['payload', 'message'],
  profileFailure: ['message'],

  editProfileRequest: ['body'],
  editProfileSuccess: ['message'],
  editProfileFailure: ['message']
});

export const ProfileTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  error: null,
  message: null,
  payload: {
    profile: null
  }
});

/* ------------- Reducers ------------- */

export const profileRequest = (state) => {
  return Object.assign({}, state, { fetching: true, error: null, message: null})
};

export const profileSuccess = (state, action) => {
  const { payload, message } = action;
  return Object.assign({}, state, { fetching: false, error: false, message, payload });
};

export const profileFailure = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: true, message });
};

export const editProfileRequest = (state, action) => {
  const { body } = action;
  return Object.assign({}, state, { fetching: true, error: null, message: null, body})
};

export const editProfileSuccess = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: false, message });
};

export const editProfileFailure = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: true, message });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PROFILE_REQUEST]: profileRequest,
  [Types.PROFILE_SUCCESS]: profileSuccess,
  [Types.PROFILE_FAILURE]: profileFailure,

  [Types.EDIT_PROFILE_REQUEST]: editProfileRequest,
  [Types.EDIT_PROFILE_SUCCESS]: editProfileSuccess,
  [Types.EDIT_PROFILE_FAILURE]: editProfileFailure
});
