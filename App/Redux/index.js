import configureStore from './Config/CreateStore'
import rootSaga from '../Sagas/'
import { persistCombineReducers } from 'redux-persist'
import immutablePersistenceTransform from '../Services/ImmutablePersistenceTransform'
import { AsyncStorage } from 'react-native'
import { reducer as network } from 'react-native-offline';
import { reducer as navReducer } from './Config/NavigationRedux'
import { reducer as loginReducer } from './Auth/LoginRedux'



import { reducer as messageReducer } from './Inbox'
import { reducer as messageDetailReducer } from './Inbox/messageDetail'
import { reducer as profileReducer } from './Profile'
import { reducer as sendMessageReducer } from './Compose'
import { reducer as deleteMessageReducer } from './Inbox/deleteMessage'




const config = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['nav'],
  transform: [immutablePersistenceTransform],
};

//assembling reducer
export const reducers = persistCombineReducers(config, {
  network,
  nav: navReducer,
  auth: loginReducer,


  message: messageReducer,
  messageDetail: messageDetailReducer,
  profile: profileReducer,
  sendMessage: sendMessageReducer,
  deleteMessage: deleteMessageReducer



});

export default () => {
  let { persistor, store, networkStart } = configureStore(reducers, rootSaga);

  

  return {persistor, store, networkStart}
}
