import {combineReducers} from 'redux';
import {NavigationActions} from 'react-navigation';

import AppNavigation from '../../Navigation/AppNavigation';

// Start with two routes: The Main screen, with the Login screen on top.

const secondAction = AppNavigation.router.getActionForPathAndParams('Login');

/**
 * Creates an navigation action for dispatching to Redux.
 *
 * @param {string} routeName The name of the route to go to.
 */
// const navigateTo = routeName => () => navigate({ routeName })
const INITIAL_STATE = AppNavigation.router.getStateForAction(
  secondAction
);


export function reducer(state = INITIAL_STATE, action) {
  let nextState;
  switch (action.type) {
    case 'LOGOUT':
      nextState = AppNavigation.router.getStateForAction(
        NavigationActions.navigate({routeName: 'Login'}),
      );
      break;
    case 'AUTO_LOGIN':
      if (action.role === "tutor") {
        nextState = AppNavigation.router.getStateForAction(
          NavigationActions.navigate({routeName: 'TutorHome'}),
        );
      } else if (action.role === "admin") {
        nextState = AppNavigation.router.getStateForAction(
          NavigationActions.navigate({routeName: 'AdminHome'}),
        );
      } else if (action.role === "parent") {
        nextState = AppNavigation.router.getStateForAction(
          NavigationActions.navigate({routeName: 'OrangTuaHome'}),
        );
      } else {
        nextState = AppNavigation.router.getStateForAction(
          NavigationActions.navigate({routeName: 'Login'}),
        );
      }
      break;
    default:
      nextState = AppNavigation.router.getStateForAction(action, state);
      break;
  }


  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
