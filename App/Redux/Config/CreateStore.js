import { AsyncStorage, Platform, NetInfo } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux'
import Config from '../../Config/DebugConfig'
import createSagaMiddleware from 'redux-saga'
import ScreenTracking from './ScreenTrackingMiddleware'
import { persistStore } from 'redux-persist'
import { createNetworkMiddleware, offlineActionTypes } from 'react-native-offline';
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

function isNetworkConnected(): Promise<boolean> {
  if (Platform.OS === 'ios') {
    return new Promise(resolve => {
      const handleFirstConnectivityChangeIOS = isConnected => {
        NetInfo.isConnected.removeEventListener( // Cleaning up after initial detection
          'connectionChange',
          handleFirstConnectivityChangeIOS,
        );
        resolve(isConnected);
      };
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        handleFirstConnectivityChangeIOS,
      );
    });
  }

  return NetInfo.isConnected.fetch();
}

// creates the store
export default (rootReducer, rootSaga) => {
  /* ------------- Redux Configuration ------------- */

  const middleware = []
  const enhancers = []

  /* ------------- Analytics Middleware ------------- */
  // middleware.push(ScreenTracking)

  /* ------------- Saga Middleware ------------- */

  const navMiddleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
  );
  const sagaMonitor = Config.useReactotron ? console.tron.createSagaMonitor() : null
  const sagaMiddleware = createSagaMiddleware({ sagaMonitor })
  const networkMiddleware = createNetworkMiddleware();
  middleware.push(networkMiddleware, sagaMiddleware, navMiddleware);

  /* ------------- Assemble Middleware ------------- */

  enhancers.push(applyMiddleware(...middleware))

  // if Reactotron is enabled (default for __DEV__), we'll create the store through Reactotron
  const createAppropriateStore = Config.useReactotron ? console.tron.createStore : createStore
  const store = createAppropriateStore(rootReducer, compose(...enhancers))
  const networkStart = (callback) => {
    // After rehydration completes, we detect initial connection
    isNetworkConnected().then(isConnected => {
      store.dispatch({
        type: offlineActionTypes.CONNECTION_CHANGE,
        payload: isConnected,
      });
      callback(); // Notify our root component we are good to go, so that we can render our app
    });
  };

  const persistor = persistStore(store, null, store.getState);
  // kick off root saga
  let sagasManager = sagaMiddleware.run(rootSaga)

  return {
    persistor,
    store,
    sagasManager,
    sagaMiddleware,
    networkStart
  }
}
