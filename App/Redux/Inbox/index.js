import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  messageRequest: null,
  messageSuccess: ['payload', 'message'],
  messageFailure: ['message'],
});

export const messageTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  error: null,
  message: null,
  payload: {
    data: null
  }
});

/* ------------- Reducers ------------- */

export const messageRequest = (state) => {
  return Object.assign({}, state, { fetching: true, error: null, message: null});
};

export const messageSuccess = (state, action) => {
  const { payload, message } = action;
  return Object.assign({}, state, { fetching: false, error: false, message, payload });
};

export const messageFailure = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: true, message });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MESSAGE_REQUEST]: messageRequest,
  [Types.MESSAGE_SUCCESS]: messageSuccess,
  [Types.MESSAGE_FAILURE]: messageFailure
});
