import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  messageDetailRequest: ['body'],
  messageDetailSuccess: ['payload', 'message'],
  messageDetailFailure: ['message'],
});

export const messageDetailTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  error: null,
  message: null,
  payload: {
    data: null
  }
});

/* ------------- Reducers ------------- */

export const messageDetailRequest = (state, action) => {
  const { body } = action;
  return Object.assign({}, state, { fetching: true, error: null, message: null, body});
};

export const messageDetailSuccess = (state, action) => {
  const { payload, message } = action;
  return Object.assign({}, state, { fetching: false, error: false, message, payload });
};

export const messageDetailFailure = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: true, message });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MESSAGE_DETAIL_REQUEST]: messageDetailRequest,
  [Types.MESSAGE_DETAIL_SUCCESS]: messageDetailSuccess,
  [Types.MESSAGE_DETAIL_FAILURE]: messageDetailFailure
});
