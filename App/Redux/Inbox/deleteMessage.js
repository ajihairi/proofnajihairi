import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  deleteMessageRequest: ['idx'],
  deleteMessageSuccess: ['message'],
  deleteMessageFailure: ['message']
});

export const DeleteMessageTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  error: null,
  message: null,
  payload: null
});

/* ------------- Reducers ------------- */

export const deleteMessageRequest = (state, action) => {
  const {idx} = action;
  return Object.assign({}, state, {fetching: true, error: null, message: null, idx})
};

export const deleteMessageSuccess = (state, action) => {
  const {message} = action;
  return Object.assign({}, state, {fetching: false, error: false, message});
};

export const deleteMessageFailure = (state, action) => {
  const {message} = action;
  return Object.assign({}, state, {fetching: false, error: true, message});
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DELETE_MESSAGE_REQUEST]: deleteMessageRequest,
  [Types.DELETE_MESSAGE_SUCCESS]: deleteMessageSuccess,
  [Types.DELETE_MESSAGE_FAILURE]: deleteMessageFailure,
});
