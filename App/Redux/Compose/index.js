import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  sendMessageRequest: ['body'],
  sendMessageSuccess: ['message'],
  sendMessageFailure: ['message']
});

export const SendMessageTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  error: null,
  message: null,
  payload: {
    message: null
  }
});

/* ------------- Reducers ------------- */

export const sendMessageRequest = (state, action) => {
  const { body } = action;
  return Object.assign({}, state, { fetching: true, error: null, message: null, body})
};

export const sendMessageSuccess = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: false, message });
};

export const sendMessageFailure = (state, action) => {
  const { message } = action;
  return Object.assign({}, state, { fetching: false, error: true, message });
};

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SEND_MESSAGE_REQUEST]: sendMessageRequest,
  [Types.SEND_MESSAGE_SUCCESS]: sendMessageSuccess,
  [Types.SEND_MESSAGE_FAILURE]: sendMessageFailure
});
